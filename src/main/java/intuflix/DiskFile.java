package intuflix;

import java.nio.file.Path;

public class DiskFile {
    public Path file;
    public String fileHex;
    public String h0;

    public DiskFile(Path file, String fileHex, String h0) {
        this.file = file;
        this.fileHex = fileHex;
        this.h0 = h0;
    }
}
