package intuflix;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Encoder {

    Logger logger = LoggerFactory.getLogger(Encoder.class);

    private static final int CHUNK_SIZE = 1024;

    public InMemoryFile encodeInMemory(String file) throws IOException, NoSuchAlgorithmException {
        byte[] array = getBytes(file);

        ArrayList<byte[]> blocks = createReveresedBlocks(array);

        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        byte[] h0 = digest.digest(blocks.get(0));
        byte [] encodedByteArray = new byte[0];
        byte [] first = blocks.get(0);
        invertArray(first);
        encodedByteArray = concatArrays(encodedByteArray, first);
        for (int i = 1; i < blocks.size(); i++) {
            byte [] block = blocks.get(i);
            byte [] chunk = concatArrays(block,h0);
            digest = MessageDigest.getInstance("SHA-256");
            h0 = digest.digest(chunk);
            invertArray(chunk);
            encodedByteArray = concatArrays(encodedByteArray, chunk);
        }
        digest = MessageDigest.getInstance("SHA-256");
        invertArray(encodedByteArray);
        return new InMemoryFile(encodedByteArray, byteArrayToHex(digest.digest(encodedByteArray)), byteArrayToHex(h0));
    }

    public DiskFile encodeToDisk(String file) throws NoSuchAlgorithmException, IOException {
        List<Path> blocks = createBlocks(file);
        Collections.reverse(blocks);

        List<Path> chunks = new ArrayList<>();

        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        byte[] first = getBytes(blocks.get(0));
        byte[] h0 = digest.digest(first);
        Path tempFile = Files.createTempFile("chunk-"+chunks.size()+"-", ".bin");
        Files.write(tempFile, first, StandardOpenOption.CREATE);
        chunks.add(tempFile);

        for (int i = 1; i < blocks.size(); i++) {
            byte [] block = getBytes(blocks.get(i));
            byte [] chunk = concatArrays(block,h0);
            tempFile = Files.createTempFile("block-"+chunks.size()+"-", ".bin");
            Files.write(tempFile, chunk, StandardOpenOption.CREATE);
            chunks.add(tempFile);

            digest = MessageDigest.getInstance("SHA-256");
            h0 = digest.digest(chunk);
        }

        Collections.reverse(chunks);
        Path encodedFile = Files.createTempFile("encodedFile", ".bin");
        digest = MessageDigest.getInstance("SHA-256");
        for (Path chunk : chunks) {
            byte[] bytes = getBytes(chunk);
            digest.update(bytes);
            Files.write(encodedFile, bytes, StandardOpenOption.APPEND);
        }

        for (Path block : blocks) {
            Files.deleteIfExists(block);
        }
        for (Path chunk : chunks) {
            Files.deleteIfExists(chunk);
        }

        return new DiskFile(encodedFile, byteArrayToHex(digest.digest()), byteArrayToHex(h0));
    }

    void invertArray(byte[] array) {
        for (int i = 0; i < array.length / 2; i++) {
            byte temp = array[i];
            array[i] = array[array.length - 1 - i];
            array[array.length - 1 - i] = temp;
        }
    }



    public static String byteArrayToHex(byte[] a) {
        StringBuilder sb = new StringBuilder(a.length * 2);
        for(byte b: a)
            sb.append(String.format("%02x", b));
        return sb.toString();
    }

    private byte[] concatArrays(byte[] a, byte[] b) {
        byte [] concat = new byte[a.length+b.length];
        System.arraycopy(a, 0, concat, 0, a.length);
        System.arraycopy(b, 0, concat, a.length, b.length);
        return concat;
    }

    private void appendToFile(String file, byte[] content) throws IOException {
        Files.write(
                Paths.get(file),
                content,
                StandardOpenOption.APPEND, StandardOpenOption.CREATE);
    }

    private byte[] getBytes(String file) throws IOException {
        URL url = getClass().getClassLoader().getResource(file);
        return getBytes(Paths.get(url.getPath()));
    }

    private byte[] getBytes(Path path) throws IOException {
        return Files.readAllBytes(path);
    }

    private ArrayList<byte[]> createReveresedBlocks(byte[] array) {
        ArrayList<byte[]> chunks = new ArrayList<>();

        int left = 0;
        int right = 1024;
        while (right <= array.length-1){
            chunks.add(Arrays.copyOfRange(array, left, right));
            left+=1024;
            right+=1024;
        }
        if (right > array.length){
            right=array.length;
            chunks.add(Arrays.copyOfRange(array, left, right));
        }

        Collections.reverse(chunks);
        return chunks;
    }

    /**
     * Encode a file
     * @return - Path of encoded file
     */

    public List<Path> createBlocks(String file) {
        List<Path> blocks = new ArrayList<>();
        try(RandomAccessFile reader = new RandomAccessFile(getClass().getClassLoader().getResource(file).getPath(), "r");
            FileChannel channel = reader.getChannel();
            ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            ByteBuffer buff = ByteBuffer.allocate(CHUNK_SIZE);
            while (channel.read(buff) > 0) {
                // create a temporary file
                Path tempFile = Files.createTempFile("block-"+blocks.size()+"-", ".bin");
                Files.write(tempFile, Arrays.copyOfRange(buff.array(), 0, buff.position()), StandardOpenOption.APPEND);
                blocks.add(tempFile);
                buff.clear();
            }
        }catch (IOException ioe){
            throw new RuntimeException(ioe);
        }
        return blocks;

    }

}
