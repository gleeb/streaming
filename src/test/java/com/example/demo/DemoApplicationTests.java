package com.example.demo;

import intuflix.Decoder;
import intuflix.DiskFile;
import intuflix.Encoder;
import intuflix.InMemoryFile;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

class DemoApplicationTests {

	private static final String FILE_NAME = "zero.bin";

	@Test
	void inMemoryFileEncoderSanityTest(){
		Encoder encoder = new Encoder();
		InMemoryFile inMemoryFile = null;
		try {
			inMemoryFile = encoder.encodeInMemory(FILE_NAME);
		} catch (IOException | NoSuchAlgorithmException e) {
			Assert.fail();
		}
		Assert.assertEquals("e6e19106304b9665184e957d5dbf58ad36c55b52bf8d993dae4340c96bd8e81f", inMemoryFile.h0);
		Assert.assertEquals("3857d5a53a726b96fe0de7f1b7bd0239c56aae4a5f9b9a75428cff6f850aeb56", inMemoryFile.fileHex);
	}

	@Test
	void inMemoryDecoderSanityTest(){
		Encoder encoder = new Encoder();
		InMemoryFile inMemoryFile = null;
		try {
			inMemoryFile = encoder.encodeInMemory(FILE_NAME);
		} catch (IOException | NoSuchAlgorithmException e) {
			Assert.fail();
		}
		Assert.assertEquals("e6e19106304b9665184e957d5dbf58ad36c55b52bf8d993dae4340c96bd8e81f", inMemoryFile.h0);
		Assert.assertEquals("3857d5a53a726b96fe0de7f1b7bd0239c56aae4a5f9b9a75428cff6f850aeb56", inMemoryFile.fileHex);

		Decoder decoder = new Decoder();
		try {
			decoder.decodeInMomory(inMemoryFile);
		} catch (NoSuchAlgorithmException e) {
			Assert.fail();
		}
	}

	@Test
	void validateH0InMemoryIntegrity() {
		Encoder encoder = new Encoder();
		InMemoryFile inMemoryFile = null;
		try {
			inMemoryFile = encoder.encodeInMemory(FILE_NAME);
		} catch (IOException | NoSuchAlgorithmException e) {
			Assert.fail();
		}
		Assert.assertEquals("e6e19106304b9665184e957d5dbf58ad36c55b52bf8d993dae4340c96bd8e81f", inMemoryFile.h0);
		Assert.assertEquals("3857d5a53a726b96fe0de7f1b7bd0239c56aae4a5f9b9a75428cff6f850aeb56", inMemoryFile.fileHex);

		Decoder decoder = new Decoder();
		inMemoryFile.h0="123";
		InMemoryFile finalInMemoryFile = inMemoryFile;
		Assert.assertThrows(RuntimeException.class, ()->decoder.decodeInMomory(finalInMemoryFile));
	}

	@Test
	void testInMemoryDecoder_fail(){
		Encoder encoder = new Encoder();
		InMemoryFile inMemoryFile = null;
		try {
			inMemoryFile = encoder.encodeInMemory(FILE_NAME);
		} catch (IOException | NoSuchAlgorithmException e) {
			Assert.fail();
		}
		Assert.assertEquals("e6e19106304b9665184e957d5dbf58ad36c55b52bf8d993dae4340c96bd8e81f", inMemoryFile.h0);
		Assert.assertEquals("3857d5a53a726b96fe0de7f1b7bd0239c56aae4a5f9b9a75428cff6f850aeb56", inMemoryFile.fileHex);

		Decoder decoder = new Decoder();
		byte[] h0 = new byte[32];
		new Random().nextBytes(h0);
		inMemoryFile.h0=Encoder.byteArrayToHex(h0);
		InMemoryFile finalInMemoryFile = inMemoryFile;
		Assert.assertThrows(RuntimeException.class, ()->decoder.decodeInMomory(finalInMemoryFile));
	}


	@Test
	void diskFileEncoderSanityTest(){
		Encoder encoder = new Encoder();
		DiskFile diskFile = null;
		try {
			diskFile = encoder.encodeToDisk(FILE_NAME);
		} catch (NoSuchAlgorithmException | IOException e) {
			Assert.fail();
		}
		Assert.assertEquals("e6e19106304b9665184e957d5dbf58ad36c55b52bf8d993dae4340c96bd8e81f", diskFile.h0);
		Assert.assertEquals("3857d5a53a726b96fe0de7f1b7bd0239c56aae4a5f9b9a75428cff6f850aeb56", diskFile.fileHex);
	}

	@Test
	void diskDecoderSanityTest(){
		Encoder encoder = new Encoder();
		DiskFile diskFile = null;
		try {
			diskFile = encoder.encodeToDisk(FILE_NAME);
		} catch (IOException | NoSuchAlgorithmException e) {
			Assert.fail();
		}
		Assert.assertEquals("e6e19106304b9665184e957d5dbf58ad36c55b52bf8d993dae4340c96bd8e81f", diskFile.h0);
		Assert.assertEquals("3857d5a53a726b96fe0de7f1b7bd0239c56aae4a5f9b9a75428cff6f850aeb56", diskFile.fileHex);

		Decoder decoder = new Decoder();
		try {
			decoder.decodeFromDisk(diskFile);
		} catch (NoSuchAlgorithmException | IOException e) {
			Assert.fail();
		}
	}

	@Test
	void validateH0DiskIntegrity() {
		Encoder encoder = new Encoder();
		DiskFile diskFile = null;
		try {
			diskFile = encoder.encodeToDisk(FILE_NAME);
		} catch (IOException | NoSuchAlgorithmException e) {
			Assert.fail();
		}
		Assert.assertEquals("e6e19106304b9665184e957d5dbf58ad36c55b52bf8d993dae4340c96bd8e81f", diskFile.h0);
		Assert.assertEquals("3857d5a53a726b96fe0de7f1b7bd0239c56aae4a5f9b9a75428cff6f850aeb56", diskFile.fileHex);

		Decoder decoder = new Decoder();
		diskFile.h0="123";
		DiskFile finalDiskFile = diskFile;
		Assert.assertThrows(RuntimeException.class, ()->decoder.decodeFromDisk(finalDiskFile));
	}

	@Test
	void testFileDecoder_fail(){
		Encoder encoder = new Encoder();
		DiskFile diskFile = null;
		try {
			diskFile = encoder.encodeToDisk(FILE_NAME);
		} catch (IOException | NoSuchAlgorithmException e) {
			Assert.fail();
		}
		Assert.assertEquals("e6e19106304b9665184e957d5dbf58ad36c55b52bf8d993dae4340c96bd8e81f", diskFile.h0);
		Assert.assertEquals("3857d5a53a726b96fe0de7f1b7bd0239c56aae4a5f9b9a75428cff6f850aeb56", diskFile.fileHex);

		Decoder decoder = new Decoder();
		byte[] h0 = new byte[32];
		new Random().nextBytes(h0);
		diskFile.h0=Encoder.byteArrayToHex(h0);
		DiskFile finalDiskFile = diskFile;
		Assert.assertThrows(RuntimeException.class, ()->decoder.decodeFromDisk(finalDiskFile));
	}

	@Test
	void checkFileIntegrity() {
		MessageDigest digest = null;
		try {
			digest = MessageDigest.getInstance("SHA-256");
		} catch (NoSuchAlgorithmException e) {
			Assert.fail();
		}
		URL url = getClass().getClassLoader().getResource(FILE_NAME);
		byte [] b = new byte[0];
		try {
			b = Files.readAllBytes(Paths.get(url.getPath()));
		} catch (IOException e) {
			Assert.fail();
		}
		Assert.assertEquals("95b532cc4381affdff0d956e12520a04129ed49d37e154228368fe5621f0b9a2", Encoder.byteArrayToHex(digest.digest(b)));
	}

}
