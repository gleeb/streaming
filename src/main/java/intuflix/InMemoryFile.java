package intuflix;

public class InMemoryFile {
    public byte[] file;
    public String fileHex;
    public String h0;

    public InMemoryFile(byte[] file, String fileHex, String h0) {
        this.file = file;
        this.fileHex = fileHex;
        this.h0 = h0;
    }
}
