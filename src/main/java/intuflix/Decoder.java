package intuflix;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * User: owolfstein
 * Date: 01-04-2021
 * Time: 13:32
 */
public class Decoder {

    public void decodeInMomory(InMemoryFile inMemoryFile) throws NoSuchAlgorithmException {
        byte[] expectedH = decodeHexString(inMemoryFile.h0);
        if (expectedH.length != 32){
            throw new RuntimeException("not valid h0");
        }
        ArrayList<byte[]> chunks = createChunks(inMemoryFile.file);
        for (int i = 0; i < chunks.size()-1; i++) {
            byte[] chunk = chunks.get(i);
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] actualH = digest.digest(chunk);
            if (Arrays.compare(expectedH, actualH) != 0) {
                throw new RuntimeException("corrupted");
            }
            expectedH = Arrays.copyOfRange(chunk, 1024, chunk.length);
        }
    }

    public void decodeFromDisk(DiskFile diskFile) throws IOException, NoSuchAlgorithmException {
        byte[] expectedH = decodeHexString(diskFile.h0);
        if (expectedH.length != 32){
            throw new RuntimeException("not valid h0");
        }

        try(RandomAccessFile reader = new RandomAccessFile(diskFile.file.toString(), "r");
            FileChannel channel = reader.getChannel();
            ByteArrayOutputStream out = new ByteArrayOutputStream()){

            ByteBuffer buff = ByteBuffer.allocate(1024+32);
            while (channel.read(buff) > 0) {
                // create a temporary file
                byte [] chunk = Arrays.copyOfRange(buff.array(), 0, buff.position());
                MessageDigest digest = MessageDigest.getInstance("SHA-256");
                byte[] actualH = digest.digest(chunk);
                if (Arrays.compare(expectedH, actualH) != 0) {
                    throw new RuntimeException("corrupted");
                }
                if (chunk.length < 1024+32){
                    return;
                }
                expectedH = Arrays.copyOfRange(chunk, 1024, chunk.length);
                buff.clear();
            }
        }
    }

    private ArrayList<byte[]> createChunks(byte[] array) {
        ArrayList<byte[]> chunks = new ArrayList<>();

        int left = 0;
        int right = 1024+32;
        while (right <= array.length-1){
            chunks.add(Arrays.copyOfRange(array, left, right));
            left+=1024+32;
            right+=1024+32;
        }
        if (right > array.length){
            right=array.length;
            chunks.add(Arrays.copyOfRange(array, left, right));
        }
        return chunks;
    }

    public byte[] decodeHexString(String hexString) {
        if (hexString.length() % 2 == 1) {
            throw new IllegalArgumentException(
                    "Invalid hexadecimal String supplied.");
        }

        byte[] bytes = new byte[hexString.length() / 2];
        for (int i = 0; i < hexString.length(); i += 2) {
            bytes[i / 2] = hexToByte(hexString.substring(i, i + 2));
        }
        return bytes;
    }

    private byte hexToByte(String hexString) {
        int firstDigit = toDigit(hexString.charAt(0));
        int secondDigit = toDigit(hexString.charAt(1));
        return (byte) ((firstDigit << 4) + secondDigit);
    }

    private int toDigit(char hexChar) {
        int digit = Character.digit(hexChar, 16);
        if(digit == -1) {
            throw new IllegalArgumentException(
                    "Invalid Hexadecimal Character: "+ hexChar);
        }
        return digit;
    }


}
